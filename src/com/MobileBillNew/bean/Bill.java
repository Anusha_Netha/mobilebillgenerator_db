package com.MobileBillNew.bean;

import java.sql.Date;

/**
 * this class is used to set,get the values
 * 
 * @author IMVIZAG
 *
 */
public class Bill {

	// declares variables
	private String mobile_number;
	private String name;
	private Date expiry_date;
	private float amount;

	// getter and setter methods
	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(Date expiry_date) {
		this.expiry_date = expiry_date;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

}
