package com.MobileBillNew.bean;

/**
 * this class is used to set, get the values
 * 
 * @author IMVIZAG
 *
 */
public class Account {
	// declaring the variables
	private String mobileNumber;
	private String network;
	private String status;

	// getter and setter methods
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
