package com.MobileBillNew.service;

import java.sql.Date;
import java.util.List;

import com.MobileBillNew.bean.Bill;
import com.MobileBillNew.dao.PostpaidBillDAO;

/**
 * This class gets the methods from the dao packagek
 * 
 * @author RANJHS
 *
 */

public class PostpaidBill {

	public static Date getExpiryDate(int activation_id) {

		Date expiry_date = PostpaidBillDAO.getExpiryDate(activation_id);
		return expiry_date;
	}

	public static String dataUsed(int activation_id) {

		String dataUsed = PostpaidBillDAO.getDataUsed(activation_id);
		return dataUsed;
	}

	public static int planId(int activation_id) {

		int plan_id = PostpaidBillDAO.getPlanId(activation_id);
		return plan_id;
	}

	public static String actualData(int plan_id) {

		String actualData = PostpaidBillDAO.getActualData(plan_id);
		return actualData;
	}

	public static float getAmount(int plan_id) {
		float amount = PostpaidBillDAO.getAmount(plan_id);
		return amount;
	}

	public static float getPriceFromDataUsage() {

		float price = PostpaidBillDAO.getPriceFromDataUsage();
		return price;
	}

	public static boolean setDataIntoTable(int activation_id,String datausage,float bill) {
		
		boolean setData = PostpaidBillDAO.setDataIntoTable(activation_id, datausage, bill);
		return setData;
		
		
	}
	public static List<Bill> displayUsers(){
		
		return PostpaidBillDAO.displayUsers();
	}
	
}
