package com.MobileBillNew.service;

import java.util.ArrayList;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.dao.AdminPlansOperations;

/**
 * This class is used to call the AdminPlansOperations methods from the DAO
 * package
 * 
 * @author RANJHS
 *
 */

public class AdminPlansService {

	// Database Connection
	ArrayList<Adminplans> al = new ArrayList<Adminplans>();

	public void doAddingPlans(Adminplans ap, int packType) {
		AdminPlansOperations dob = new AdminPlansOperations();
		dob.doAddingPlans(ap, packType);

	}

	public ArrayList<Adminplans> doDisplayPlans(int packType) {
		AdminPlansOperations dob = new AdminPlansOperations();
		al = dob.doDisplayPlans(packType);
		return al;
	}

	public void doDeletePlans(int planType, String planName) {
		AdminPlansOperations dob = new AdminPlansOperations();
		dob.doDeletePlans(planType, planName);
	}

	public boolean doValidatePlans(Adminplans ap) {
		return false;

	}

}
