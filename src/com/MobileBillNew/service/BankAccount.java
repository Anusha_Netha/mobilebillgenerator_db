package com.MobileBillNew.service;

import com.MobileBillNew.dao.BankAccountDAO;
/**
 * This class allows to access the database
 * @author IMVIZAG
 *
 */


public class BankAccount {

	public static int doGetBalance(int balance,String accountType)
	{
		int amount=BankAccountDAO.getAmount(balance,accountType);
		return amount;
	}
	public static boolean doSendBalance(int amount,int accountNumber,String accountType)
	{
		boolean result= BankAccountDAO.doSendMoney(amount,accountNumber,accountType);
		return result;
	}
	public static void addingMoneytoadminAccount( int amount,int accountNumber)
	{
		BankAccountDAO.doAddingMoneyToAddmin(amount,accountNumber);
	}
}
