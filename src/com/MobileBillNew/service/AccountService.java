package com.MobileBillNew.service;

import java.util.List;

import com.MobileBillNew.bean.Account;
import com.MobileBillNew.dao.AccountDAO;
/**
 * this class calls the DAO methods
 * @author IMVIZAG
 *
 */
public class AccountService {
	
	public static boolean setUserSwitchRequest(String mobilenumber,String network) {
		String status = "working";
		boolean result = AccountDAO.setSwitchUserRequest(mobilenumber, network,status);
		return result;
	}
	public static List<Account> fetchSwitchAccountData() {
		
		List<Account> list = AccountDAO.getSwitchUsers();
		return list;
	}
	public static boolean updateDetails(String mobilenumber,String network,String status)
	{
		String switchedNetwork = null;
		boolean result = false;
		if(network.equals("prepaid")) {
			switchedNetwork = "postpaid";
			if( AccountDAO.setDetails(mobilenumber, switchedNetwork, status)) {
				if(AccountDAO.updateUserNetwork(mobilenumber, switchedNetwork)) {
					result = AccountDAO.deleteActivationforSwitchedUser(mobilenumber);
				}
				
			}
		}
		else if(network.equals("postpaid")) {
			switchedNetwork = "prepaid";
			if(AccountDAO.setDetails(mobilenumber, switchedNetwork, status)) {
				if(AccountDAO.updateUserNetwork(mobilenumber, switchedNetwork)) {
					result = AccountDAO.deleteActivationforSwitchedUser(mobilenumber);
				}
				
			}
		}
		return result;
	}
}
