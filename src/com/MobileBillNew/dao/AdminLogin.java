package com.MobileBillNew.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.MobileBillNew.util.DBUtil;

/**
 * This class contains the login method and forgot password to allow a admin to
 * login
 * 
 * @author RANJHS
 */
public class AdminLogin implements AdminLoginImpl {

	/**
	 * this method helps the admin to set the username from the database
	 */

	@Override
	public boolean doLogin(String username) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "select username from admin where username = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * this method helps the admin to set the username and password from the
	 * database
	 * 
	 * @param username sets the username
	 * @param password sets the password
	 * @return it returns the result to the service package
	 */

	public boolean doLogin(String username, String password) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "select username,password from admin where username = ? and password = ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * this method is used to update the password if the admin forgets it
	 * 
	 * @param username sets the username
	 * @param password sets the password
	 * @return it returns the result to the service package
	 */

	public boolean updatePassword(String username, String password) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "update admin set password = ? where username = ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, password);
			ps.setString(2, username);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * this is method is used to get the security question from database if a user
	 * wants to reset their password
	 * 
	 * @param securityanswer
	 * @param username
	 * @return
	 */
	public boolean doSecurityanswerDAO(String securityanswer, String username) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "select username,securityAnswer from admin where username = ? and securityAnswer= ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, securityanswer);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = true;
			}

		} catch (SQLException e) {
			// System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

}
