package com.MobileBillNew.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.MobileBillNew.bean.Account;
import com.MobileBillNew.util.DBUtil;

/**
 * this class is used to access to the database
 * 
 * @author IMVIZAG
 *
 */
public class AccountDAO {

	/**
	 * this method sets the switch request of the users
	 * 
	 * @param mobilenumber
	 * @param network
	 * @param status
	 * @return
	 */
	public static boolean setSwitchUserRequest(String mobilenumber, String network, String status) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		try {
			con = DBUtil.getCon();
			String sql = "insert into switchrequest(mobilenumber,network,status) values(?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setString(1, mobilenumber);
			ps.setString(2, network);
			ps.setString(3, status);
			int res = ps.executeUpdate();
			if (res != 0) {
				result = true;
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		return result;

	}

	/**
	 * this method gets all the switch requests from the database to admin
	 * 
	 * @return
	 */
	public static List<Account> getSwitchUsers() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Account account = null;
		List<Account> list = new ArrayList<Account>();
		try {
			con = DBUtil.getCon();
			String sql = "select * from switchrequest where status = 'working'";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				account = new Account();
				account.setMobileNumber(rs.getString(2));
				account.setNetwork(rs.getString(3));
				account.setStatus(rs.getString(4));
				list.add(account);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		return list;

	}

	/**
	 * this method sets the details if admin accepts/deny's the request
	 * 
	 * @param mobilenumber
	 * @param network
	 * @param status
	 * @return
	 */
	public static boolean setDetails(String mobilenumber, String network, String status) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		try {
			con = DBUtil.getCon();
			String sql = "update switchrequest set network = ?,status = ? where mobilenumber = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, network);
			ps.setString(2, status);
			ps.setString(3, mobilenumber);
			int res = ps.executeUpdate();
			if (res != 0) {
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		return result;

	}

	/**
	 * this method is used to update the users network after network switch
	 * 
	 * @param mobilenumber
	 * @param network
	 * @return
	 */
	public static boolean updateUserNetwork(String mobilenumber, String network) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		try {
			con = DBUtil.getCon();
			String sql = "update service set network = ? where mobile_number = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, network);
			ps.setString(2, mobilenumber);
			int res = ps.executeUpdate();
			if (res != 0) {
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		return result;

	}

	/**
	 * this method deletes the activated plans if a user is switched
	 * 
	 * @param mobileNumber
	 * @return
	 */
	public static boolean deleteActivationforSwitchedUser(String mobileNumber) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		try {
			con = DBUtil.getCon();
			String sql = "delete from activated_users where users_id = (select users_id from users where mobile_number = ?)";
			ps = con.prepareStatement(sql);
			ps.setString(1, mobileNumber);
			int res = ps.executeUpdate();
			if (res != 0) {
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		return result;
	}

}
