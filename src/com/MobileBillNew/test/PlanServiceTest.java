package com.MobileBillNew.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.service.PlansService;

/**
 * this class is used to test the plans services
 * @author IMVIZAG
 *
 */
public class PlanServiceTest {

	PlansService plans = null;

	/**
	 * this method is used to start test case
	 */
	@Before
	public void setUp() {
		plans = new PlansService();
	}

	/**
	 * this method is used to end the test case
	 */
	@After
	public void tearDown() {
		plans = null;
	}

	/**
	 * this method is used to test to display the special plans
	 */
	@Test
	public void displaySpecialPlansTest() {
		List<Adminplans> list = plans.displaySpecialPlans();
		assertNotNull(list);
	}

	@Ignore
	@Test
	public void displaySpecialPlansNegativeTest() {
		List<Adminplans> list = plans.displaySpecialPlans();
		assertEquals(null,list);
	}
	
	@Test
	public void displayDataPlansTest() {
		List<Adminplans> list = plans.displayDataPlans();
		assertNotNull(list);
	}
	
	@Ignore
	@Test
	public void displayDataPlansNegativeTest() {
		List<Adminplans> list = plans.displayDataPlans();
		assertEquals(null,list);
	}
	
	@Test
	public void displayTalktimePlansTest() {
		List<Adminplans> list = plans.displayTalktimePlans();
		assertNotNull(list);
	}
	
	@Ignore
	@Test
	public void displayTalktimePlansNegativeTest() {
		List<Adminplans> list = plans.displayTalktimePlans();
		assertEquals(null,list);
	}

	@Test
	public void displayUnlimitedPlansTest() {
		List<Adminplans> list = plans.displayUnlimitedPlans();
		assertNotNull(list);
	}
	
	@Ignore
	@Test
	public void displayUnlimitedPlansNegativeTest() {
		List<Adminplans> list = plans.displayUnlimitedPlans();
		assertEquals(null,list);
	}
	
	@Test
	public void displayPostpaidPlansTest() {
		List<Adminplans> list = plans.displayPostpaidPlans();
		assertNotNull(list);
	}
	
	@Ignore
	@Test
	public void dispalyPostpaidPlansNegativeTest() {
		List<Adminplans> list = plans.displayPostpaidPlans();
		assertEquals(null,list);
	}
}
