package com.MobileBillNew.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.MobileBillNew.bean.Bill;
import com.MobileBillNew.service.PostpaidBill;

/**
 * this class is used to test postpaidbill generation
 * @author IMVIZAG
 *
 */
public class PostpaidBillTest {
	
	PostpaidBill bill = null;
	
	@Before
	public void setUp() {
		bill = new PostpaidBill();
	}
	
	@After
	public void tearDown() {
		bill = null;
	}
	
	@Test
	public void getExpiryDateTest() {
		Date expiry_date = bill.getExpiryDate(3);
		assertNotNull(expiry_date);
	}
	
	@Test
	public void getExpiryDateNegativeTest() {
		Date expiry_date = bill.getExpiryDate(0);
		assertEquals(null,expiry_date);
	}
	
	@Test
	public void dataUsedTest() {
		String dataUsed = bill.dataUsed(3);
		assertNotNull(dataUsed);
	}
	
	@Test
	public void dataUsedNegativeTest() {
		String dataUsed = bill.dataUsed(0);
		assertEquals(null,dataUsed);
	}
	
	@Test
	public void planIdTest() {
		int plan_id = bill.planId(3);
		assertNotNull(plan_id);
	}
	
	@Test
	public void planIdNegativeTest() {
		int plan_id = bill.planId(0);
		assertEquals(0,plan_id);
	}
	
	@Test
	public void actualDataTest() {
		String actualData = bill.actualData(3);
		assertNotNull(actualData);
	}
	
	@Test
	public void actualDataNegativeTest() {
		String actualData = bill.actualData(0);
		assertEquals(null,actualData);
	}
	
	@Test
	public void getAmountTest() {
		float amount = bill.getAmount(3);
		assertNotNull(amount);
	}
	
	@Test
	public void getAmountNegativeTest() {
		float amount = bill.getAmount(0);
		assertTrue(0.0 == amount);
	}
	
	@Test
	public void getPriceFromDataUsageTest() {
		float amount = bill.getPriceFromDataUsage();
		assertNotNull(amount);
	}
	
	@Ignore
	@Test
	public void getPriceFromDataUsageNegativeTest() {
		float amount = bill.getPriceFromDataUsage();
		assertTrue(0.0 == amount);
	}

	@Test
	public void setDataintoTableTest() {
		boolean setData = bill.setDataIntoTable(3, "60GB", 199);
		assertTrue(setData);
	}
	
	@Ignore
	@Test
	public void setDataintoTableNegativeTest() {
		boolean setData = bill.setDataIntoTable(0,"0.0",199);
		assertFalse(setData);
	}
	
	@Test
	public void displayUsersTest() {
		List<Bill> list = bill.displayUsers();
		assertNotNull(list);
	}
	
	@Ignore
	@Test
	public void displayUsersNegativeTest() {
		List<Bill> list = bill.displayUsers();
		assertEquals(null,list);
	}
}
