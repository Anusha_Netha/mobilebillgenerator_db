package com.MobileBillNew.controller;
import java.util.Random;
import java.util.Scanner;

/**
 * this class is used to generate otp 
 * @author IMVIZAG
 *
 */
public class OtpGeneration {

	/**
	 * this method  is used to generate otp if user number is valid
	 * @return true if user entered correct otp else false
	 */
	public static boolean otpGen() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		boolean flag = false;
		int count = 1;

		// declaring the predefined Random class object for generating OTP
		Random rd = new Random();
		int otp = rd.nextInt(10000);
		System.out.println(otp);
		if (otp < 0) {
			otp = otp * (-1);
		}

		// entered OTP is correct
		System.out.println("enter the otp");
		int enterOtp = 0;
		try {
			enterOtp = sc.nextInt();
		} catch(Exception e) {
			System.out.println("Only Integer Values");
		}
		

		if (enterOtp == otp) {
			flag = true;
			count = 3;

		}
		// entered OTP is wrong, iterates for 2 times
		while (count != 3) {
			System.out.println("wrong entered otp!!! re-enter for " + count + " time");
			@SuppressWarnings("unused")
			int enterOtp1 = 0;
			try {
				enterOtp1 = sc.nextInt();
			} catch(Exception e) {
				System.out.println("Only Integer Values");
			}
			// if user enters correct OTP
			if (enterOtp == otp) {
				flag = true;
				break;
			}
			flag = false;
			count = count + 1;

		}

		// returns true or false based on the input(OTP) given by the user
		return flag;

	}
}
