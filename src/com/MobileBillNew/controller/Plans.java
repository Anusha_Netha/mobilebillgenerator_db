package com.MobileBillNew.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.dao.BankAccountDAO;
import com.MobileBillNew.service.ActivatedUsers;
import com.MobileBillNew.service.BankAccount;
import com.MobileBillNew.service.PlansService;

/**
 * Used to display the plans
 * 
 * @author RANJHS
 *
 */

public class Plans {

	static int count = 1;

	/**
	 * This method is used to display the plans
	 * 
	 * @param list
	 * @param service_id
	 */
	static int countDisplay=0;
	static int countInvalid=0;
	static int accountNumberPaytm=9949370;
	static int accountNumberGooglepay=12345;
	static int accountNumberAdmin=62253522;
	static String accountType="";
	
	public static void displaysplans(List<Adminplans> list, int service_id) {

		Scanner sc = new Scanner(System.in);
		Iterator<Adminplans> i = list.iterator();
		while (i.hasNext()) {
			Adminplans adminplans = i.next();
			System.out.println((count++) + "." + " " + adminplans.getPlan_name() + " " + adminplans.getCalls() + "calls"
					+ " " + adminplans.getData() + "GB" + " " + adminplans.getAmazon_prime() + " "
					+ adminplans.getNetfix() + " " + adminplans.getAmount() + " " + adminplans.getValidity() + "days");
		}
		System.out.println("Choose your Plan");
		System.out.println("-------------------------------------------------------------------------------------------------------------");

		int plan_option = 0;
		
		try {
			plan_option = sc.nextInt();
		}catch(Exception e)
		{
			if(countDisplay == 2 )
			{		System.out.println("-------------------------------------------------------------------------------------------------------------");
					
					System.out.println("You have entered wrong input so many times please try after sometime!!");
					System.exit(0);
			}
			countDisplay++;
			System.out.println("input is invalid");
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			
			displaysplans(list,service_id);
		}
		
		if (plan_option <= (list.size())) {
			
			//Start of transactions		
			System.out.println("Are you sure you want to Actiavte the Plan(yes/no)");
			String option = sc.next();
			if (option.equalsIgnoreCase("yes")) {
				Adminplans plan = list.get(plan_option - 1);
				System.out.println("-------------------------------------------------------------------------------------------------------------");
				
				System.out.println("Pay of amount: "+plan.getAmount());
				
				
				int amount = 0;
				int balanceOfUser=0;
				System.out.println("-------------------------------------------------------------------------------------------------------------");
				
				System.out.println("Choose Account:");
				System.out.println("1.Paytm \n2.Googlepay");
				String chooseAccount=sc.next();
				
				switch(chooseAccount)
				{
					case "1":accountType="paytm";
						amount=(int)plan.getAmount();
						 balanceOfUser=BankAccount.doGetBalance(accountNumberPaytm,accountType);
						break;
					case "2": accountType="googlepay";
					amount=(int)plan.getAmount();
					 balanceOfUser=BankAccount.doGetBalance(accountNumberGooglepay,accountType);					
					break;
					default:System.out.println("in correct credential please try again");
					System.exit(0);
				}
				
				System.out.println("Wait for transactions....");
				System.out.println("-------------------------------------------------------------------------------------------------------------");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(amount>balanceOfUser)
				{
					System.out.println("In suffiecient balance");
					System.exit(0);
				}
				int remainingBalance=balanceOfUser-amount;
				
				int addingMoneyToAdminAccount=balanceOfUser+amount;				
				BankAccount.addingMoneytoadminAccount(addingMoneyToAdminAccount,accountNumberAdmin);
				boolean checkTransaction=BankAccountDAO.doSendMoney(remainingBalance,accountNumberGooglepay,accountType);
				if(!checkTransaction)
				{
					System.out.println("You transaction failed due to some problem ");
					System.exit(0);
				}
				
				//end of transactions
				
				
				System.out.println("Your Plan is Activated with " + plan.getPlan_name()
						+ " and validity of the plan is " + plan.getValidity());
				
				
				Date activation_date = new Date(new java.util.Date().getTime());
				// System.out.println(activation_date);
				Date date = new Date(new java.util.Date().getTime());
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				cal.add(Calendar.DATE, plan.getValidity());
				java.util.Date dt = cal.getTime();
				Date expiry_date = new Date(dt.getTime());
				// System.out.println(expiry_date);
				int users_id = ActivatedUsers.getUsersId(service_id);
				if (ActivatedUsers.plansActivation(plan, activation_date, expiry_date, users_id)) {
					System.out.println("Activated Successfully");
					
				} else {
					System.out.println("Activation is not successfull due to technical problem");
				}
			} else {
				System.out.println("Thank you");
			}
		}
		else {
			if(countInvalid==2)
			{
				System.out.println("Too many attempts try after some time");
				System.exit(0);
			}
				countInvalid++;
			System.out.println("Input is invalid");
			
			displaysplans(list,service_id);
		}

		sc.close();
	}

	/**
	 * It displays the PrePaid Plans
	 * 
	 * @param service_id
	 */
	static int countChoice=0;
	public static void display_PrepaidPlans(int service_id) {
		List<Adminplans> list = new ArrayList<Adminplans>();
		Scanner sc = new Scanner(System.in);
		System.out.println("1. Special Plans");
		System.out.println("2. Data Plans");
		System.out.println("3. Talktime Plans");
		System.out.println("4. Unlimited Plans");
		System.out.println("choose your pack");
		System.out.println("-------------------------------------------------------------------------------------------------------------");

		int plan_option = 0;
		try {
			plan_option = sc.nextInt();
		}catch(Exception e)
		{
			if(countChoice == 2 )
			{		System.out.println("-------------------------------------------------------------------------------------------------------------");
					
					System.out.println("You have entered wrong input so many times please try after sometime!!");
					System.exit(0);
			}
			countChoice++;
			System.out.println("input is invalid");
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			
			display_PrepaidPlans(service_id);
		}
		switch (plan_option) {
		case 1:
			list = PlansService.displaySpecialPlans();
			if (list.isEmpty()) {
				System.out.println("Sorry!!! No Packs");
			} else {
				displaysplans(list, service_id);

			}

			break;
		case 2:
			list = PlansService.displayDataPlans();
			if (list.isEmpty()) {
				System.out.println("Sorry!!! No Packs");
			} else {
				displaysplans(list, service_id);

			}
			break;
		case 3:
			list = PlansService.displayTalktimePlans();
			if (list.isEmpty()) {
				System.out.println("Sorry!!! No Packs");
			} else {
				displaysplans(list, service_id);
			}
			break;
		case 4:
			list = PlansService.displayUnlimitedPlans();
			if (list.isEmpty()) {
				System.out.println("Sorry!!! No Packs");
			} else {
				displaysplans(list, service_id);

			}

			break;
		default:
			System.out.println("Choose valid Option");
		}
		sc.close();
	}

	/**
	 * It displays the PrePaid Plans
	 * 
	 * @param service_id
	 */

	public static void display_PostpaidPlans(int service_id) {
		List<Adminplans> list = new ArrayList<Adminplans>();
		list = PlansService.displayPostpaidPlans();
		displaysplans(list, service_id);

	}
}
