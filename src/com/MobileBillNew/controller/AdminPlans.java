package com.MobileBillNew.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.service.AdminPlansService;

/**
 * This class is used to display all the plans and sends the bill for the
 * PostPaid Users
 * 
 * @author RANJHS
 *
 */

public class AdminPlans {
	static int packType;
	static AdminPlansService as = new AdminPlansService();
	static int countController = 0;
	static int countCatchdisplay = 0;
	static int countDisplayplans = 0;
	static int countOfferplans = 0;

	/**
	 * This method is used to display the Plans and send the bill
	 */

	public static void plans() {
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");

		doDisplayplans();
	}

	public static void doWelcome() {
		System.out.println("			Thank you");
		System.out.println();
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");
		System.out.println("                                             WELCOME TO OUR NETWORK");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");
		System.out.println();
	}

	public static void doDisplayplans() {
		Scanner sc = new Scanner(System.in);
		System.out.println("1.PrepaidUnlimitedplans ");
		System.out.println("2.PrepaidTalktimeplans ");
		System.out.println("3.PrepaidDataplans ");
		System.out.println("4.PrepaidSpecialplans ");
		System.out.println("5.Postpaidplans ");
		System.out.println("6.GoBack");
		System.out.println("0.GoHome");
		int choice2 = 0;
		try {
			choice2 = sc.nextInt();
		} catch (Exception e) {
			System.out.println("Wrong Input");
		}
		switch (choice2) {
		case 0:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			Main.mainDisplay();
		case 1:
			unlimitedPlans();
			break;
		case 2:
			talkTimePlans();
			break;

		case 3:
			dataPlans();
			break;
		case 4:
			specialPlans();
			break;
		case 5:
			postPaidPlans();

			break;
		case 6:
			plans();
			break;
		default:
			if (countOfferplans == 2) {
				System.out.println("You have entered wrong input so many times please try after sometime!!");
			}
			countOfferplans++;
			doDisplayplans();
			System.out.println("wrong input entered ");
		}
		sc.close();

	}

	public static void unlimitedPlans() {
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");

		Scanner sc = new Scanner(System.in);
		System.out.println("1.AddPrepaidUnlimitedplans ");
		System.out.println("2.DisplayPrepaidUnlimitedplans");
		System.out.println("3.DeletePrepaidUnlimitedplans");
		System.out.println("0.GoBack");
		int choicePrepaidUnlimitedPlans = 0;
		try {
			choicePrepaidUnlimitedPlans = sc.nextInt();
		} catch (Exception e) {
			System.out.println("Invalid Input");
		}
		packType = 5;
		switch (choicePrepaidUnlimitedPlans) {

		case 0:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			doDisplayplans();
			break;
		case 1:
			System.out.println("do u want to add new offers to the network....if Yes enter NUMBER...else enter 0");
			System.out.println("Enter the number How many offers you will add :");
			int number = sc.nextInt();

			Adminplans ap = null;

			for (int i = 1; i <= number; i++) {
				ap = new Adminplans();
				System.out.println("Enter Plan name :");
				ap.setPlan_name(sc.next());

				System.out.println("Enter plan data :");
				ap.setData(sc.next());

				System.out.println("Enter calls:");
				ap.setCalls(sc.nextInt());
				System.out.println("Enter Amazon prime:");
				ap.setAmazon_prime(sc.next());
				System.out.println("Enter netflix:");
				ap.setNetfix(sc.next());
				System.out.println("Enter validity:");
				ap.setValidity(sc.nextInt());
				System.out.println("Enter amount:");
				ap.setAmount(sc.nextDouble());

				as.doAddingPlans(ap, packType);

			}
			System.out.println("Successfully added in database");
			System.out.println("If you want to Go back enter 0");
			int enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				unlimitedPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}

			break;
		case 2:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			ArrayList<Adminplans> al = new ArrayList<Adminplans>();
			al = as.doDisplayPlans(packType);
			Iterator<Adminplans> it = al.iterator();
			System.out.println(
					"Plan Name          	Data           	Calls             AmazonPrime              Netflix         	Amount               validity           	PlanType    ");
			System.out.println(
					"====================================================================================================================================================================");
			while (it.hasNext()) {
				Adminplans ap1 = (Adminplans) it.next();
				// System.out.printf("%-20d %-20s %-20s \n", b.getOfferName(),
				// b.getOfferDescription(),b.getValidity());
				System.out.printf("%-20s %-20s %-20d %-20s %-20s %-20f %-20d %-20s \n", ap1.getPlan_name(),
						ap1.getData(), ap1.getCalls(), ap1.getAmazon_prime(), ap1.getNetfix(), ap1.getAmount(),
						ap1.getValidity(), ap1.getPackType());
			}
			System.out.println();
			System.out.println(
					"====================================================================================================================================================================");

			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				unlimitedPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}
			break;
		case 3:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			System.out.print("Welcome to Delete plans");
			System.out.println("Enter plan name");
			String planName = sc.next();
			as.doDeletePlans(packType, planName);

			System.out.println("Successfully deleted in database");
			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				unlimitedPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}
			break;
		default:
			System.out.println("wrong input entered ");

		}
		sc.close();
	}

	public static void talkTimePlans() {
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");

		Scanner sc = new Scanner(System.in);
		System.out.println("1.AddPrepaidTalktimeplans ");
		System.out.println("2.DisplayPrepaidTalktimeplans");
		System.out.println("3.UpdatePrepaidTalktimeplans");
		System.out.println("0.GoBack");
		packType = 3;
		int choicePrepaidTalktimePlans = sc.nextInt();
		switch (choicePrepaidTalktimePlans) {
		case 0:
			doDisplayplans();
			break;
		case 1:
			System.out.println("do u want to add new offers to the network....if Yes enter NUMBER...else enter 0");
			System.out.println("Enter the number How many offers you will add :");
			int number = sc.nextInt();
			Adminplans ap = null;

			for (int i = 1; i <= number; i++) {
				ap = new Adminplans();
				System.out.println("Enter Plan name :");
				ap.setPlan_name(sc.next());

				System.out.println("Enter plan data :");
				ap.setData(sc.next());

				System.out.println("Enter calls:");
				ap.setCalls(sc.nextInt());
				System.out.println("Enter Amazon prime:");
				ap.setAmazon_prime(sc.next());
				System.out.println("Enter netflix:");
				ap.setNetfix(sc.next());
				System.out.println("Enter validity:");
				ap.setValidity(sc.nextInt());
				System.out.println("Enter amount:");
				ap.setAmount(sc.nextDouble());

				as.doAddingPlans(ap, packType);
			}
			System.out.println("Successfully added in database");
			System.out.println("If you want to Go back enter 0");
			int enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				talkTimePlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}

			break;
		case 2:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			ArrayList<Adminplans> al = new ArrayList<Adminplans>();
			al = as.doDisplayPlans(packType);
			Iterator<Adminplans> it = al.iterator();
			System.out.println(
					"Plan Name          	Data           	Calls             AmazonPrime              Netflix         	Amount               validity           	PlanType    ");
			System.out.println(
					"====================================================================================================================================================================");
			while (it.hasNext()) {
				Adminplans ap1 = (Adminplans) it.next();
				// System.out.printf("%-20d %-20s %-20s \n", b.getOfferName(),
				// b.getOfferDescription(),b.getValidity());
				System.out.printf("%-20s %-20s %-20d %-20s %-20s %-20f %-20d %-20s \n", ap1.getPlan_name(),
						ap1.getData(), ap1.getCalls(), ap1.getAmazon_prime(), ap1.getNetfix(), ap1.getAmount(),
						ap1.getValidity(), ap1.getPackType());
			}

			System.out.println();
			System.out.println(
					"====================================================================================================================================================================");

			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				talkTimePlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}
			break;

		case 3:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			System.out.print("Welcome to Delete plans");
			System.out.println("Enter plan name");
			int planType = 1;
			String planName = sc.next();
			as.doDeletePlans(planType, planName);

			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				talkTimePlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}
			break;
		default:
			System.out.println("wrong input entered ");

		}
		sc.close();
	}

	public static void dataPlans() {
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");

		Scanner sc = new Scanner(System.in);
		System.out.println("1.AddPrepaidDataplans ");
		System.out.println("2.DisplayPrepaidDataplans");
		System.out.println("3.DeletePrepaidDataplans");
		System.out.println("0.GoBack");
		packType = 2;
		int choicePrepaidDataPlans = sc.nextInt();
		switch (choicePrepaidDataPlans) {
		case 0:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			doDisplayplans();
			break;
		case 1:
			System.out.println("do u want to add new offers to the network....if Yes enter NUMBER...else enter 0");
			System.out.println("Enter the number How many offers you will add :");
			int number = sc.nextInt();
			Adminplans ap = null;

			for (int i = 1; i <= number; i++) {
				ap = new Adminplans();
				System.out.println("Enter Plan name :");
				ap.setPlan_name(sc.next());

				System.out.println("Enter plan data :");
				ap.setData(sc.next());

				System.out.println("Enter calls:");
				ap.setCalls(sc.nextInt());
				System.out.println("Enter Amazon prime:");
				ap.setAmazon_prime(sc.next());
				System.out.println("Enter netflix:");
				ap.setNetfix(sc.next());
				System.out.println("Enter validity:");
				ap.setValidity(sc.nextInt());
				System.out.println("Enter amount:");
				ap.setAmount(sc.nextDouble());

				as.doAddingPlans(ap, packType);
			}
			System.out.println("Successfully added in database");
			System.out.println("If you want to Go back enter 0");
			int enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				dataPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}

			break;
		case 2:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			ArrayList<Adminplans> al = new ArrayList<Adminplans>();
			al = as.doDisplayPlans(packType);
			Iterator<Adminplans> it = al.iterator();
			System.out.println(
					"Plan Name          	Data           	Calls             AmazonPrime              Netflix         	Amount               validity           	PlanType    ");
			System.out.println(
					"====================================================================================================================================================================");
			while (it.hasNext()) {
				Adminplans ap1 = (Adminplans) it.next();
				// System.out.printf("%-20d %-20s %-20s \n", b.getOfferName(),
				// b.getOfferDescription(),b.getValidity());
				System.out.printf("%-20s %-20s %-20d %-20s %-20s %-20f %-20d %-20s \n", ap1.getPlan_name(),
						ap1.getData(), ap1.getCalls(), ap1.getAmazon_prime(), ap1.getNetfix(), ap1.getAmount(),
						ap1.getValidity(), ap1.getPackType());
			}
			System.out.println();
			System.out.println(
					"====================================================================================================================================================================");

			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				dataPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}

			break;
		case 3:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			System.out.print("Welcome to Delete plans");
			System.out.println("Enter plan name");
			packType = 3;
			String planName = sc.next();
			as.doDeletePlans(packType, planName);

			System.out.println("Successfully deleted in database");
			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				dataPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}

			break;
		default:
			System.out.println("wrong input entered ");

		}
		sc.close();
	}

	public static void specialPlans() {
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");

		Scanner sc = new Scanner(System.in);
		System.out.println("1.AddPrepaidSpecialplans ");
		System.out.println("2.DisplayPrepaidSpecialplans");
		System.out.println("3.DeletePrepaidSpecialplans");
		System.out.println("0.GoBack");
		packType = 1;
		int choicePrepaidSpecialPlans = sc.nextInt();
		switch (choicePrepaidSpecialPlans) {
		case 0:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			doDisplayplans();
			break;
		case 1:
			System.out.println("do u want to add new offers to the network....if Yes enter NUMBER...else enter 0");
			System.out.println("Enter the number How many offers you will add :");
			int number = sc.nextInt();
			Adminplans ap = null;

			for (int i = 1; i <= number; i++) {
				ap = new Adminplans();
				System.out.println("Enter Plan name :");
				ap.setPlan_name(sc.next());

				System.out.println("Enter plan data :");
				ap.setData(sc.next());

				System.out.println("Enter calls:");
				ap.setCalls(sc.nextInt());
				System.out.println("Enter Amazon prime:");
				ap.setAmazon_prime(sc.next());
				System.out.println("Enter netflix:");
				ap.setNetfix(sc.next());
				System.out.println("Enter validity:");
				ap.setValidity(sc.nextInt());
				System.out.println("Enter amount:");
				ap.setAmount(sc.nextDouble());

				as.doAddingPlans(ap, packType);
			}
			System.out.println("Successfully added in database");
			System.out.println("If you want to Go back enter 0");
			int enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				specialPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}

			break;
		case 2:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");
			ArrayList<Adminplans> al = new ArrayList<Adminplans>();
			al = as.doDisplayPlans(packType);
			Iterator<Adminplans> it = al.iterator();
			System.out.println(
					"Plan Name          	Data           	Calls             AmazonPrime              Netflix         	Amount               validity           	PlanType    ");
			System.out.println(
					"====================================================================================================================================================================");
			while (it.hasNext()) {
				Adminplans ap1 = (Adminplans) it.next();
				// System.out.printf("%-20d %-20s %-20s \n", b.getOfferName(),
				// b.getOfferDescription(),b.getValidity());
				System.out.printf("%-20s %-20s %-20d %-20s %-20s %-20f %-20d %-20s \n", ap1.getPlan_name(),
						ap1.getData(), ap1.getCalls(), ap1.getAmazon_prime(), ap1.getNetfix(), ap1.getAmount(),
						ap1.getValidity(), ap1.getPackType());
			}
			System.out.println();
			System.out.println(
					"====================================================================================================================================================================");

			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				specialPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}

			break;
		case 3:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");
			System.out.print("Welcome to Delete plans");
			System.out.println("Enter plan name");
			String planName = sc.next();
			as.doDeletePlans(packType, planName);
			System.out.println("Successfully deleted in database");
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				specialPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}
			break;
		default:
			System.out.println("wrong input entered ");

		}
		sc.close();
	}

	public static void postPaidPlans() {
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");

		Scanner sc = new Scanner(System.in);
		packType = 4;
		System.out.println("1.AddPostpaidplans ");
		System.out.println("2.DisplaypostPaidplans");
		System.out.println("3.Updatepostpaidplans");
		System.out.println("0.GoBack");
		int choiceUpdatePostPaidPlans = 0;
		try {
			choiceUpdatePostPaidPlans = sc.nextInt();
		} catch (Exception e) {
			System.out.println("Invalid Input");
		}
		switch (choiceUpdatePostPaidPlans) {
		case 0:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			doDisplayplans();
			break;
		case 1:
			System.out.println("do u want to add new offers to the network....if Yes enter NUMBER...else enter 0");
			System.out.println("Enter the number How many offers you will add :");
			int number = sc.nextInt();
			Adminplans ap = null;

			for (int i = 1; i <= number; i++) {
				ap = new Adminplans();
				System.out.println("Enter Plan name :");
				ap.setPlan_name(sc.next());

				System.out.println("Enter plan data :");
				ap.setData(sc.next());

				System.out.println("Enter calls:");
				ap.setCalls(sc.nextInt());
				System.out.println("Enter Amazon prime:");
				ap.setAmazon_prime(sc.next());
				System.out.println("Enter netflix:");
				ap.setNetfix(sc.next());
				System.out.println("Enter validity:");
				ap.setValidity(sc.nextInt());
				System.out.println("Enter amount:");
				ap.setAmount(sc.nextDouble());

				as.doAddingPlans(ap, packType);
			}
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			System.out.println("Successfully added in database");
			System.out.println("If you want to Go back enter 0");
			int enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				postPaidPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}
			break;
		case 2:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			ArrayList<Adminplans> al = new ArrayList<Adminplans>();
			al = as.doDisplayPlans(packType);
			Iterator<Adminplans> it = al.iterator();
			System.out.println(
					"Plan Name          	Data           	Calls             AmazonPrime              Netflix         	Amount               validity           	PlanType    ");
			System.out.println(
					"====================================================================================================================================================================");
			while (it.hasNext()) {
				Adminplans ap1 = (Adminplans) it.next();
				// System.out.printf("%-20d %-20s %-20s \n", b.getOfferName(),
				// b.getOfferDescription(),b.getValidity());
				System.out.printf("%-20s %-20s %-20d %-20s %-20s %-20f %-20d %-20s \n", ap1.getPlan_name(),
						ap1.getData(), ap1.getCalls(), ap1.getAmazon_prime(), ap1.getNetfix(), ap1.getAmount(),
						ap1.getValidity(), ap1.getPackType());
			}
			System.out.println();
			System.out.println(
					"====================================================================================================================================================================");
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				postPaidPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}

			break;
		case 3:
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			System.out.print("Welcome to Delete plans");
			System.out.println("Enter plan name");
			String planName = sc.next();
			as.doDeletePlans(packType, planName);
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");

			System.out.println("Successfully deleted in database");
			System.out.println("If you want to Go back enter 0");
			enterData = sc.nextInt();
			if (enterData == 0) {
				System.out.println(
						"-------------------------------------------------------------------------------------------------------------");

				postPaidPlans();
			} else {
				System.out.println("Sorry your entered wrong credential");
			}
			break;
		default:
			System.out.println("wrong input entered ");

		}
		sc.close();
	}
}
