package com.MobileBillNew.controller;

import java.util.Scanner;

/**
 * this is main class which enables user and admin to login and activate their
 * plans
 * 
 * @author IMVIZAG
 *
 */
public class Main {

	static int packType;
	static int count = 0;
	static int countCatch = 0;

	/**
	 * this is the method from where the execution starts
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println();
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");
		System.out.println("                                             WELCOME TO HAR");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");
		System.out.println();
		mainDisplay();

	}

	/**
	 * this method is used to display the details from where a user and admin will
	 * login
	 */
	public static void mainDisplay() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Your Choice");
		System.out.println();
		System.out.println("1. Admin");
		System.out.println("2. User Login");
		System.out.println();
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------");
		int choice = 0;
		try {
			choice = sc.nextInt();
		} catch (Exception e) {
			System.out.println("the input is  not valid");
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");
			if (countCatch == 2) {
				System.out.println("Too many times attempt please try again after sometime");
				System.exit(0);
			}
			countCatch++;
			mainDisplay();
			System.exit(0);
		}
		switch (choice) {

		case 1:
			AdminRegistration.adminRegistration();

			break;
		case 2:
			UserLogin.doLogin();
			break;
		default:
			System.out.println("wrong input entered ");
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");
			if (count == 2) {
				System.out.println("Too many times attempt please try again after sometime");
				System.exit(0);
			}
			count++;
			mainDisplay();

		}
		sc.close();
	}
}
