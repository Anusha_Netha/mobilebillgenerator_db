-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: mobile
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `account_id` int(10) NOT NULL,
  `account_name` varchar(20) DEFAULT NULL,
  `balance` float DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (62253522,'Admin',10021),(62253544,'User',9979);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activated_users`
--

DROP TABLE IF EXISTS `activated_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activated_users` (
  `activation_id` int(11) NOT NULL AUTO_INCREMENT,
  `plans_id` int(11) DEFAULT NULL,
  `activation_date` date DEFAULT NULL,
  `Expiry_date` date DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`activation_id`),
  KEY `users_id` (`users_id`),
  KEY `plans_id` (`plans_id`),
  CONSTRAINT `activated_users_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`users_id`),
  CONSTRAINT `activated_users_ibfk_2` FOREIGN KEY (`plans_id`) REFERENCES `plans` (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activated_users`
--

LOCK TABLES `activated_users` WRITE;
/*!40000 ALTER TABLE `activated_users` DISABLE KEYS */;
INSERT INTO `activated_users` VALUES (3,9,'2018-12-08','2019-01-05',6),(5,3,'2019-01-04','2019-02-01',7),(6,3,'2019-01-04','2019-02-01',7),(9,3,'2019-01-05','2019-02-02',9),(10,6,'2019-01-05','2019-01-06',10),(11,3,'2019-01-05','2019-02-02',10),(12,3,'2019-01-05','2019-02-02',11),(13,5,'2019-01-05','2019-02-02',12),(15,8,'2019-01-06','2019-02-03',2);
/*!40000 ALTER TABLE `activated_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activated_users1`
--

DROP TABLE IF EXISTS `activated_users1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activated_users1` (
  `activated_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) DEFAULT NULL,
  `dataUsage` text,
  `calls` int(11) NOT NULL,
  `activation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`activated_id`),
  KEY `activated_user_id_foreign` (`plan_id`),
  KEY `activated` (`activation_id`),
  CONSTRAINT `activated` FOREIGN KEY (`activation_id`) REFERENCES `activated_users` (`activation_id`) ON DELETE CASCADE,
  CONSTRAINT `activated_user_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`plan_id`) ON DELETE CASCADE,
  CONSTRAINT `activated_users1_ibfk_1` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`plan_id`),
  CONSTRAINT `activated_users1_ibfk_2` FOREIGN KEY (`activation_id`) REFERENCES `activated_users` (`activation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activated_users1`
--

LOCK TABLES `activated_users1` WRITE;
/*!40000 ALTER TABLE `activated_users1` DISABLE KEYS */;
INSERT INTO `activated_users1` VALUES (1,9,'150GB',1000,3),(5,3,NULL,0,5),(10,3,NULL,0,10),(12,5,NULL,0,13),(14,8,NULL,0,15);
/*!40000 ALTER TABLE `activated_users1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL,
  `security_answer` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'ramu','ramu@123','vijay'),(3,'ramu12345','ramu12345','vijau'),(4,'anu_netha','anusha123','prabhas');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datausage`
--

DROP TABLE IF EXISTS `datausage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datausage` (
  `data_usage_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_usage_name` varchar(20) NOT NULL,
  `price` float NOT NULL,
  `mbps` int(11) NOT NULL,
  PRIMARY KEY (`data_usage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datausage`
--

LOCK TABLES `datausage` WRITE;
/*!40000 ALTER TABLE `datausage` DISABLE KEYS */;
INSERT INTO `datausage` VALUES (1,'data',10,10),(2,'calls',2,1);
/*!40000 ALTER TABLE `datausage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `googlepay`
--

DROP TABLE IF EXISTS `googlepay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googlepay` (
  `googlepay_id` int(10) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`googlepay_id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `googlepay_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `googlepay`
--

LOCK TABLES `googlepay` WRITE;
/*!40000 ALTER TABLE `googlepay` DISABLE KEYS */;
INSERT INTO `googlepay` VALUES (13456,62253544);
/*!40000 ALTER TABLE `googlepay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paytm`
--

DROP TABLE IF EXISTS `paytm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paytm` (
  `paytm_id` int(10) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`paytm_id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `paytm_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paytm`
--

LOCK TABLES `paytm` WRITE;
/*!40000 ALTER TABLE `paytm` DISABLE KEYS */;
INSERT INTO `paytm` VALUES (9949370,62253544);
/*!40000 ALTER TABLE `paytm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plan_category`
--

DROP TABLE IF EXISTS `plan_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_category` (
  `plan_category_id` int(11) NOT NULL,
  `pack_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`plan_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan_category`
--

LOCK TABLES `plan_category` WRITE;
/*!40000 ALTER TABLE `plan_category` DISABLE KEYS */;
INSERT INTO `plan_category` VALUES (1,'specialplans'),(2,'dataplans'),(3,'talktimeplans'),(4,'postpaidplans'),(5,'unlimitedplans');
/*!40000 ALTER TABLE `plan_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plans`
--

DROP TABLE IF EXISTS `plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plans` (
  `plan_id` int(10) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(25) NOT NULL,
  `data` text NOT NULL,
  `calls` int(11) NOT NULL,
  `amazon_prime` varchar(30) DEFAULT NULL,
  `netflix` varchar(30) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `validity` int(11) NOT NULL,
  `plan_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`plan_id`),
  KEY `plan_category_id` (`plan_category_id`),
  CONSTRAINT `plans_ibfk_1` FOREIGN KEY (`plan_category_id`) REFERENCES `plan_category` (`plan_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plans`
--

LOCK TABLES `plans` WRITE;
/*!40000 ALTER TABLE `plans` DISABLE KEYS */;
INSERT INTO `plans` VALUES (3,'RC199','unlimited',100,'28days','28days',199,28,1),(5,'RC249','unlimited',200,'28days','28days',249,28,1),(6,'1','1',1,'1','1',1,1,2),(7,'RC51','28DAYS',100,'NO','NO',51,28,3),(8,'RC21','100MB',20,'NO','NO',21,28,3),(9,'RC399','100GB',1000,'28days','28days',399,28,4),(11,'rc399','1.4Gb',100,'28days','28days',399,28,5);
/*!40000 ALTER TABLE `plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `postpaidbill`
--

DROP TABLE IF EXISTS `postpaidbill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postpaidbill` (
  `postpaidbill_id` int(11) NOT NULL AUTO_INCREMENT,
  `activation_id` int(11) DEFAULT NULL,
  `DataUsage` text NOT NULL,
  `bill` float NOT NULL,
  PRIMARY KEY (`postpaidbill_id`),
  KEY `activation_id` (`activation_id`),
  CONSTRAINT `postpaidbill_ibfk_1` FOREIGN KEY (`activation_id`) REFERENCES `activated_users` (`activation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `postpaidbill`
--

LOCK TABLES `postpaidbill` WRITE;
/*!40000 ALTER TABLE `postpaidbill` DISABLE KEYS */;
INSERT INTO `postpaidbill` VALUES (1,3,'150GB',399);
/*!40000 ALTER TABLE `postpaidbill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `service_id` int(11) NOT NULL,
  `mobile_number` varchar(20) DEFAULT NULL,
  `network` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  UNIQUE KEY `mobile_number` (`mobile_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'9700401589','postpaid'),(2,'9177995290','prepaid'),(3,'9502199202','postpaid'),(4,'9121719350','prepaid'),(5,'7729955884','prepaid'),(6,'9949730470','prepaid'),(7,'9533535343','prepaid'),(8,'9611390804','prepaid'),(9,'7382993840','postpaid'),(10,'9575624134','postpaid'),(11,'9553560709','postpaid'),(12,'7451236984','prepaid'),(13,'8555802533','prepaid'),(14,'8978160259','prepaid'),(15,'8466840323','postpaid'),(16,'7989200302','prepaid'),(17,'9966266177','prepaid');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `switchrequest`
--

DROP TABLE IF EXISTS `switchrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `switchrequest` (
  `switchRequestId` int(11) NOT NULL AUTO_INCREMENT,
  `mobileNumber` varchar(10) DEFAULT NULL,
  `network` varchar(8) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`switchRequestId`),
  UNIQUE KEY `mobileNumber` (`mobileNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `switchrequest`
--

LOCK TABLES `switchrequest` WRITE;
/*!40000 ALTER TABLE `switchrequest` DISABLE KEYS */;
INSERT INTO `switchrequest` VALUES (1,'9700401589','postpaid','approved'),(3,'9553560709','postpaid','approved'),(4,'9121719350','prepaid','approved');
/*!40000 ALTER TABLE `switchrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `users_id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_number` varchar(10) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `gender` varchar(6) NOT NULL,
  `dob` date DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`users_id`),
  UNIQUE KEY `mobile_number` (`mobile_number`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'9553560709','raj','male','1995-10-02',11,'blocked'),(2,'9121719350','jyothi','female','1997-10-10',4,'active'),(4,'9502199202','supriya','female','1996-12-14',3,'active'),(5,'9700401589','anusha','female','1996-04-02',1,'active'),(6,'9575624134','sam','female','1995-02-01',10,'blocked'),(7,'9177995290','harshitha','female','1997-02-10',2,'active'),(8,'7729955884','ramu','male','1994-05-03',5,'active'),(9,'7989200302','Lavanya','female','1997-11-14',16,'active'),(10,'9966266177','khaza','male','1996-04-14',17,'active'),(11,'8978160259','naidu','male','1997-12-22',14,'active'),(12,'8555802533','sita','female','1996-05-08',13,'active');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-08 16:37:06
